$('.btn-food').click(function(){
    $('.caterogy .popup').addClass('show');
    $('.overlay').addClass('show');
});

$('.overlay').click(function(){
    $(this).removeClass('show');
    $('.caterogy .popup').removeClass('show');
});

$('.bi-x-lg').click(function(){
    $('.caterogy .popup').removeClass('show');
    $('.overlay').removeClass('show');
});

var $temp = $("<input>");
var $url = $(location).attr('href');

$('.btn-share').on('click', function() {
  $("body").append($temp);
  $temp.val($url).select();
  document.execCommand("copy");
  $temp.remove();
  $('p').addClass('show');
});

setTimeout(function() {
    $('.share p').fadeOut('fast');
}, 3000);

var $slider = $('.product-detail .list');

if ($slider.length) {
  var currentSlide;
  var slidesCount;
  var sliderCounter = document.createElement('div');
  sliderCounter.classList.add('slider__counter');
  
  var updateSliderCounter = function(slick, currentIndex) {
    currentSlide = slick.slickCurrentSlide() + 1;
    slidesCount = slick.slideCount;
    $(sliderCounter).text(currentSlide + '/' +slidesCount)
  };

  $slider.on('init', function(event, slick) {
    $slider.append(sliderCounter);
    updateSliderCounter(slick);
  });

  $slider.on('afterChange', function(event, slick, currentSlide) {
    updateSliderCounter(slick, currentSlide);
  });

  $slider.slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots:false,
    arrows:false,
  });
}

$('.product .list').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    dots:false,
    arrows:false,
});